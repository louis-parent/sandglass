mod hourglass;
mod app;

use notan::prelude::*;

#[notan_main]
fn main() -> Result<(), String> {
    app::run(128, 256)
}