use crate::hourglass::{Cell, Hourglass };

use std::path::PathBuf;

use notan::prelude::*;
use notan::draw::*;

static HOURGLASS_ICON_PATH: &str = "assets/icon.png";
static HOURGLASS_SIZE: (usize, usize) = (16, 32);
static FALLING_DELAY: f32 = 0.016666667;

#[derive(AppState)]
struct State {
    hourglass: Hourglass,
    since_last_tick: f32
}

pub fn run(width: u32, height: u32) -> Result<(), String> {
    notan::init_with(init)
    .draw(draw)
    .update(update)
    .add_config(DrawConfig)
    .add_config(
        WindowConfig::default()
        .set_window_icon(Some(PathBuf::from(HOURGLASS_ICON_PATH)))
        .set_taskbar_icon(Some(PathBuf::from(HOURGLASS_ICON_PATH)))
        .set_title("Sandglass")

        .set_min_size(width, height)
        .set_size(width, height)
        .set_resizable(true)
        .set_fullscreen(false)
        .set_maximized(false)
        
        .set_transparent(true)
        .set_vsync(true)
        .set_high_dpi(true)
    ).build()
}

fn init(_gfx: &mut Graphics) -> State {
    State {
        hourglass: Hourglass::new(HOURGLASS_SIZE.0, HOURGLASS_SIZE.1),
        since_last_tick: 0.0
    }
}

fn update(app: &mut App, state: &mut State) {
    if state.since_last_tick >= FALLING_DELAY {
        state.since_last_tick = 0.0;
        state.hourglass.tick();
    }
    else {
        state.since_last_tick += app.timer.delta_f32();
    }
}

fn draw(_app: &mut App, gfx: &mut Graphics, state: &mut State) {
    let mut draw = gfx.create_draw();
    draw.clear(Color::TRANSPARENT);

    let cell_width = gfx.size().0 as f32 / (HOURGLASS_SIZE.0 + 1) as f32;
    let cell_height = gfx.size().1 as f32 / (HOURGLASS_SIZE.1 + 1) as f32;

    for y in 0..state.hourglass.size.height {
        for x in 0..state.hourglass.size.width {
            let cell = state.hourglass.cell(x, y);

            if let Cell::Border = cell {
                draw.rect(
                    (
                        x as f32 * cell_width,
                        y as f32 * cell_height
                    ),
                    (
                        cell_width,
                        cell_height
                    )
                ).fill_color(Color::GRAY);
            }
            else if let Cell::Sand(seed) = cell {
                let horizontal_radius = cell_width / 2.0;
                let vertical_radius = cell_height / 2.0;
        
                draw.ellipse(
                    (
                        (x as f32 * cell_width) + horizontal_radius,
                        (y as f32 * cell_height) + vertical_radius
                    ),
                    (
                        horizontal_radius,
                        vertical_radius
                    )
                ).fill_color(Color::from_bytes(
                    255, 
                    205 + ((seed as f32 / usize::MAX as f32) * 50.0) as u8, 
                    0, 
                    255
                ));
            }
        }
    }

    gfx.render(&draw);
}