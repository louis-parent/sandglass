use std::{cmp, fmt};

///////////
// UTILS //
///////////

#[derive(Debug, Clone, Copy, Default)]
pub struct Size {
    pub width: usize,
    pub height: usize
}

///////////////
// HOURGLASS //
///////////////

#[derive(Debug, Clone, Copy, Default, PartialEq, Eq)]
pub enum Cell {
    #[default]
    Empty,
    Border,
    Sand(usize)
}

impl fmt::Display for Cell {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", match self {
            Cell::Empty => " ",
            Cell::Border => "#",
            Cell::Sand(_) => ".",
        })
    }
}


type HourglassRow = Vec<Cell>;
type HourglassGrid = Vec<HourglassRow>;
#[derive(Debug)]
pub struct Hourglass {
    pub size: Size,
    grid: HourglassGrid
}

impl Hourglass {
    pub fn new(width: usize, height: usize) -> Hourglass {
        let size = Size {
            width: if width % 2 == 0 { width + 1 } else { width },
            height: if height % 2 == 0 { height + 1 } else { height }
        };

        Hourglass {
            grid: Hourglass::generate_startup_grid(&size),
            size
        }
    }

    pub fn cell(&self, x: usize, y: usize) -> Cell {
        self.grid[y][x]
    }

    pub fn tick(&mut self) -> bool {
        let mut modified = false;

        for y in (0..self.size.height).rev() {
            for x in (0..self.size.width).rev() {
                if let Cell::Sand(seed) = self.grid[y][x] {
                    let bottom_y = y + 1;
            
                    let offset_x: isize = if rand::random() { 1 } else { -1 };
                    let first_bottom_x = (x as isize + offset_x) as usize;
                    let second_bottom_x = (x as isize - offset_x) as usize;
            
                    if self.grid[bottom_y][x] == Cell::Empty {
                        self.grid[bottom_y][x] = Cell::Sand(seed);
                        self.grid[y][x] = Cell::Empty;
                        modified = true;
                    }
                    else if self.grid[y][first_bottom_x] == Cell::Empty && self.grid[bottom_y][first_bottom_x] == Cell::Empty {
                        self.grid[bottom_y][first_bottom_x] = Cell::Sand(seed);
                        self.grid[y][x] = Cell::Empty;
                        modified = true;
                    }
                    else if self.grid[y][second_bottom_x] == Cell::Empty && self.grid[bottom_y][second_bottom_x] == Cell::Empty {
                        self.grid[bottom_y][second_bottom_x] = Cell::Sand(seed);
                        self.grid[y][x] = Cell::Empty;
                        modified = true;
                    }
                }
            }
        }

        modified
    }

    fn generate_startup_grid(size: &Size) -> HourglassGrid {
        let mut grid = Hourglass::generate_empty_grid(size);

        let mut y = 1;
        let mut next_sand_row = size.width - 2;

        while next_sand_row > 0 {
            for x in ((size.width / 2) - (next_sand_row / 2))..((size.width / 2) + (next_sand_row / 2) + 1) {
                grid[y][x] = Cell::Sand(rand::random());
            }

            if next_sand_row as isize - 2 < 0 {
                next_sand_row = 0;
            }
            else {
                next_sand_row -= 2;
            }
            y += 1;
        }

        grid
    }

    fn generate_empty_grid(size: &Size) -> HourglassGrid {
        let mut grid = std::iter::repeat(std::iter::repeat(Cell::Empty).take(size.width).collect::<Vec<Cell>>()).take(size.height).collect::<Vec<Vec<Cell>>>();
        
        let max_reduction_size = size.width / 2;
        let middle_row: usize = size.height / 2;


        for y in 0..size.height {            
            let border_offset = cmp::max(max_reduction_size as isize - (middle_row as isize - y as isize).abs(), 0) as usize;
            let left_border = cmp::max(border_offset as isize - 1, 0) as usize;
            let right_border = cmp::min(size.width - border_offset, size.width - 1);

            for x in 0..size.width {
                // Top and Bottom border
                if y == 0 || y == (size.height - 1) {
                    grid[y][x] = Cell::Border;
                }
                // Left and right border
                else if x == left_border || x == right_border {
                    grid[y][x] = Cell::Border;
                }
            }
        }

        grid
    }
}

impl fmt::Display for Hourglass {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for y in 0..self.size.height {
            for x in 0..self.size.width {
                let res = write!(f, "{}", self.grid[y][x]);
                if let Result::Err(_) = res {
                    return res;
                }
            }

            let res = write!(f, "{}", "\n");
            if let Result::Err(_) = res {
                return res;
            }
        }

        Result::Ok(())
    }
}