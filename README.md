# Sandglass

![Hourglass Icon](assets/icon.png)

**Sandglass** is an hourglass application using sand falling simulation.

This project is inspired by the [Coding Challenge n°180](https://www.youtube.com/watch?v=L4u7Zy_b868&t=964s) from [The Coding Train](https://thecodingtrain.com/).

## Simulation

The simulation uses a simple automata like system. The simulation area is represented as a grid with each cell having a state among *Empty*, *Border* or *Sand*.

At each tick the grid is iterated in reverse order. For each cell having the *Sand* state, the cell just below is checked. If it's empty the state are switched. Else the diagonals are checked the same way.